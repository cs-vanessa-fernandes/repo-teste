/*
 Projeto: conciliation
 Author/Empresa: Rede
 Copyright (C) 2016 Redecard S.A.
 */

describe('rc-select directive', function(){

    beforeEach(module('Conciliador'));
    beforeEach(module('app/views/directives/rc-select.html'));

	var $scope, element, html, vm, controller;

	beforeEach(function () {

		html = angular.element([
			"<rc-select ",
				"label=\"número do estabelecimento\" ",
				"place-holder-label=\"estabelecimento\" ",
				"model=\"model\" ",
				"groupsModel=\"teste\" ",
				"footer-config=\"footerConfig\" ",
				"data=\"data\">",
			"</rc-select>"
		].join(""));

		inject(function($rootScope, $compile) {
			$scope = $rootScope.$new();
			element = $compile(html)($scope);
			$scope.$digest();
			vm = $scope.$$childHead.vm;
			controller = $scope.$$childHead;
		});

	});


	describe('placeHolder', function () {

		it("should make placeholder as: 'todos os estabelecimentos'", inject(function () {

			var arrModel = [];

			var strPlaceHolder = vm.MakePlaceHolder(arrModel);
			expect(strPlaceHolder).toBe('todos os estabelecimentos');

		}));

		it("should make placeholder as: '111111'", inject(function () {

			var arrModel = [{label: '111111'}];
			var strPlaceHolder = vm.MakePlaceHolder(arrModel);
			expect(strPlaceHolder).toBe('111111');

		}));

		it("should make placeholder as: '111111 e outro estabelecimento'", inject(function () {

			var arrModel = [{label: '111111'},{label: '22222'}];
			var strPlaceHolder = vm.MakePlaceHolder(arrModel);
			expect(strPlaceHolder).toBe('111111 e outro estabelecimento');

		}));

		it("should make placeholder as: '111111 e outros 2 estabelecimentos'", inject(function () {

			var arrModel = [{label: '111111'},{label: '22222'},{label: '33333'}];
			var strPlaceHolder = vm.MakePlaceHolder(arrModel);
			expect(strPlaceHolder).toBe('111111 e outros 2 estabelecimentos');

		}));

		it("should make placeholder as: '111111 e outros 3 estabelecimentos'", inject(function () {

			var arrModel = [{label: '111111'},{label: '22222'},{label: '33333'},{label: '44444'}];
			var strPlaceHolder = vm.MakePlaceHolder(arrModel);
			expect(strPlaceHolder).toBe('111111 e outros 3 estabelecimentos');

		}));

	});

	describe('check and uncheck all', function () {

		it("should CheckAll on model", inject(function () {

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111'
				},
				{
					label: '222222'
				},
				{
					label: '333333'
				}
			];
			vm.checkAll();
			expect(isolatedScope.model.length).toBe(3);

		}));

		it("should UncheckAll on model", inject(function () {

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111'
				},
				{
					label: '222222'
				},
				{
					label: '333333'
				}
			];
			vm.checkAll();
			expect(isolatedScope.model.length).toBe(3);

			vm.uncheckAll();
			expect(isolatedScope.model.length).toBe(0);

		}));

	});

	describe('check and uncheck single', function () {

		it("should check single on model", inject(function () {

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111',
					id: 200
				},
				{
					label: '222222',
					id: 201
				},
				{
					label: '333333',
					id: 202
				}
			];
			vm.checkOrUncheckItem(isolatedScope.data[1]);
			expect(isolatedScope.model.length).toBe(1);
			expect(isolatedScope.model[0].id).toBe(201);
			expect(isolatedScope.model[0].label).toBe('222222');

		}));

		it("should check two datas on model", inject(function () {

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111',
					id: 200
				},
				{
					label: '222222',
					id: 201
				},
				{
					label: '333333',
					id: 202
				}
			];
			vm.checkOrUncheckItem(isolatedScope.data[1]);
			vm.checkOrUncheckItem(isolatedScope.data[2]);

			expect(isolatedScope.model.length).toBe(2);

			expect(isolatedScope.model[0].id).toBe(201);
			expect(isolatedScope.model[0].label).toBe('222222');

			expect(isolatedScope.model[1].id).toBe(202);
			expect(isolatedScope.model[1].label).toBe('333333');

		}));

		it("should check two datas and uncheck one of these on model", inject(function () {

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111',
					id: 200
				},
				{
					label: '222222',
					id: 201
				},
				{
					label: '333333',
					id: 202
				}
			];
			vm.checkOrUncheckItem(isolatedScope.data[1]);
			vm.checkOrUncheckItem(isolatedScope.data[2]);

			expect(isolatedScope.model.length).toBe(2);

			expect(isolatedScope.model[0].id).toBe(201);
			expect(isolatedScope.model[0].label).toBe('222222');

			expect(isolatedScope.model[1].id).toBe(202);
			expect(isolatedScope.model[1].label).toBe('333333');

			vm.checkOrUncheckItem(isolatedScope.data[1]);
			expect(isolatedScope.model.length).toBe(1)

		}));


	});

	describe('check and uncheck group', function () {

		it("should check all single elements from group", inject(function () {

			var isolatedScope = element.isolateScope();

			isolatedScope.model = [];

			isolatedScope.data = [
				{
					label: '111111',
					id: '111111'
				},
				{
					label: '222222',
					id: '222222'
				},
				{
					label: '333333',
					id: '333333'
				}
			];

			var objGroup = {
				id: 5,
				name:"abc",
				pvs: [
					{
						code: '111111',
						id: '111111'
					},
					{
						code: '222222',
						id: '222222'
					}
				]
			};


			expect(vm.getArrCheckedGroups()).toEqual([]);
			expect(isolatedScope.model.length).toBe(0);

			vm.checkOrUncheckGroup(objGroup);

			expect(isolatedScope.model.length).toBe(2);
			expect(vm.getArrCheckedGroups()[0]).toBe('abc');


		}));

		it("should uncheck all single elements from group", inject(function () {

			var isolatedScope = element.isolateScope();

			isolatedScope.model = [];

			isolatedScope.data = [
				{
					label: '111111',
					id: '111111',
					groups: ['abc']
				},
				{
					label: '222222',
					id: '222222',
					groups: ['abc']
				},
				{
					label: '333333',
					id: '333333'
				}
			];

			var objGroup = {
				id: 5,
				name:"abc",
				pvs: [
					{
						code: '111111',
						id: '111111'
					},
					{
						code: '222222',
						id: '222222'
					}
				]
			};


			expect(vm.getArrCheckedGroups()).toEqual([]);
			expect(isolatedScope.model.length).toBe(0);

			vm.checkOrUncheckGroup(objGroup);

			expect(isolatedScope.model.length).toBe(2);
			expect(vm.getArrCheckedGroups()[0]).toBe('abc');

			vm.checkOrUncheckGroup(objGroup);
			expect(vm.getArrCheckedGroups()).toEqual([]);
			expect(isolatedScope.model.length).toBe(0);


		}));

	});

	describe('footer', function () {

		it("shouldn't have a footer", inject(function () {
			expect(element[0].querySelectorAll('.rc-select-footer').length).toBe(0);
		}));

		it("should have a footer", inject(function () {

			var isolatedScope = element.isolateScope();
			isolatedScope.footerConfig = {
				callback: function(){},
				text: 'teste'
			};
			$scope.$digest();

			expect(element[0].querySelectorAll('.rc-select-footer').length).toBe(1);

		}));

	});

  	describe('check hide or show class show-list pvList', function () {

		it("add class show-list when click input", inject(function () {

		  	$scope.class = "show-list";
			expect($scope.class).toBe('show-list');

		}));

		it("verific if show class show-list when not click input", inject(function () {

			$scope.class = "";
			expect($scope.class).toBe('');

		}));

		it("verific if show class show-list when check single on model", inject(function () {

			$scope.class = "show-list";

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111',
					id: 200
				},
				{
					label: '222222',
					id: 201
				},
				{
					label: '333333',
					id: 202
				}
			];
			vm.checkOrUncheckItem(isolatedScope.data[1]);
			expect(isolatedScope.model.length).toBe(1);
			expect(isolatedScope.model[0].id).toBe(201);
			expect(isolatedScope.model[0].label).toBe('222222');

			expect($scope.class).toBe('show-list');

		}));

		it("verific if show class show-list when check two datas on model", inject(function () {

		  	$scope.class = "show-list";

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111',
					id: 200
				},
				{
					label: '222222',
					id: 201
				},
				{
					label: '333333',
					id: 202
				}
			];
			vm.checkOrUncheckItem(isolatedScope.data[1]);
			vm.checkOrUncheckItem(isolatedScope.data[2]);

			expect(isolatedScope.model.length).toBe(2);

			expect(isolatedScope.model[0].id).toBe(201);
			expect(isolatedScope.model[0].label).toBe('222222');

			expect(isolatedScope.model[1].id).toBe(202);
			expect(isolatedScope.model[1].label).toBe('333333');

		  	expect($scope.class).toBe('show-list');

		}));

		it("verific if show class show-list when check two datas and uncheck one of these on model", inject(function () {

		  	$scope.class = "show-list";

			var isolatedScope = element.isolateScope();
			isolatedScope.model = [];
			isolatedScope.data = [
				{
					label: '111111',
					id: 200
				},
				{
					label: '222222',
					id: 201
				},
				{
					label: '333333',
					id: 202
				}
			];
			vm.checkOrUncheckItem(isolatedScope.data[1]);
			vm.checkOrUncheckItem(isolatedScope.data[2]);

			expect(isolatedScope.model.length).toBe(2);

			expect(isolatedScope.model[0].id).toBe(201);
			expect(isolatedScope.model[0].label).toBe('222222');

			expect(isolatedScope.model[1].id).toBe(202);
			expect(isolatedScope.model[1].label).toBe('333333');

			vm.checkOrUncheckItem(isolatedScope.data[1]);
			expect(isolatedScope.model.length).toBe(1);

		  	expect($scope.class).toBe('show-list');

		}));

	});

});

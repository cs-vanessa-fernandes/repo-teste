/*
 Projeto: conciliation
 Author/Empresa: Rede
 Copyright (C) 2016 Redecard S.A.
 */

describe('Session factory', function() {

    var factory;

	beforeEach(function() {
		module('Conciliador');

		inject(function(sessionFactory) {
			factory = sessionFactory;
		});
	});

	describe('on create', function() {
        it('IsAuthenticated must be false', function() {
			expect(factory.isAuthenticated()).toBe(false);
		});

		it('Create must save the user and token values', function() {
			var token = 'token';
            var user = { name: 'User', pvList: [1, 2, 3, 4] };

            factory.create(token, user);
            expect(factory.getToken()).toEqual(token);
            expect(factory.getUser()).toEqual(user);
		});
        
        it('IsAuthenticated must be true', function() {
			expect(factory.isAuthenticated()).toBe(true);
		});
	});

    describe('on destroy', function() {
        it('IsAuthenticated must be true', function() {
			expect(factory.isAuthenticated()).toBe(true);
		});

		it('Destroy must clear the user and token values', function() {
			factory.destroy();
            expect(factory.getToken()).toBeNull();
            expect(factory.getUser()).toBeNull();
		});
        
        it('IsAuthenticated must be false', function() {
			expect(factory.isAuthenticated()).toBe(false);
		});
	});
});

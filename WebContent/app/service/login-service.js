/*
	Projeto: conciliation-webapp
	Author/Empresa: Rede
	Copyright (C) 2016 Redecard S.A.
 */

"use strict";

(function() {
    angular
        .module('Conciliador.loginService', [])
        .service('loginService', LoginService);

    LoginService.$inject = ['app', 'sessionFactory', '$http', 'Request', '$window'];

    function LoginService(app, sessionFactory, $http, Request, $window) {

		/**
		 * @method validateLogin
		 * Validar dados de acesso do usuário
		 * @param {Object} objUser Objeto com valores para login e password 
		 */
		this.validateLogin = function(objUser) {
			var strUrl = app.login.endpoint + '/login';
			var objRequest = {
				login: objUser.login,
				password: objUser.password
			};

			return $http({
				url: strUrl,
				method: "POST",
				data: objRequest,
				headers: Request.setHeaders()
			});
		};

		/**
		 * @method singleSignOn
		 * @param {String} strToken token de authenticação do usuário 
		 */
		this.singleSignOn = function(strToken) {
			var strUrl = app.login.endpoint + '/singlesignon';

			var header = {
				'Content-type': 'application/json',
				'authorization': strToken
			};

			if(sessionFactory.isAuthenticated())
				sessionFactory.destroy();

			return $http({
				method: "POST",
				url: strUrl,
				headers: header
			});
		}
	};

})();
/*
	Projeto: conciliation
	Author/Empresa: Rede
	Copyright (C) 2016 Redecard S.A.
 */

"use strict";

(function() {

	angular
		.module('Conciliador.HeaderController', [])
		.controller('HeaderController', Header);

	Header.$inject = ['$scope', '$location', '$route', '$window', '$rootScope', 'sessionFactory'];

	function Header($scope, $location, $route, $window, $rootScope, sessionFactory) {

		var objVm = this;
		objVm.isActive = IsActive;
		objVm.logout = Logout;
		objVm.showMenu = sessionFactory.isAuthenticated();
        objVm.showMenu = true;

		Init();

		function Init() {
			WatchMenuState();
		}

		function IsActive(route) {
			if ($route.current.$$route.originalPath === route) {
				return true;
			}

			return false;
		}

		function Logout() {
			$rootScope.logout();
		}

		function WatchMenuState() {
			$scope.$on('$routeChangeSuccess', function() {
				objVm.showMenu = sessionFactory.isAuthenticated();
			});
		}

	}
})();

/*
	Projeto: conciliation-webapp
	Author/Empresa: Rede
	Copyright (C) 2016 Redecard S.A.
 */

"use strict";

angular.module('Conciliador.Request',[])
	.config([function () {
}])

.factory('Request', function(sessionFactory) {

	return {
		setHeaders: SetHeaders
	};

	function SetHeaders() {

		var objDefaultHeaders = {
			'Content-Type': 'application/json'
		};

		if(sessionFactory.isAuthenticated()) {
			objDefaultHeaders.Authorization = sessionFactory.getToken();
		}

		return objDefaultHeaders;
	}

});

/*
	Projeto: conciliation
	Author/Empresa: Rede
	Copyright (C) 2016 Redecard S.A.
 */

"use strict";

(function(storage) {

	angular
		.module('Conciliador.sessionFactory', [])
		.factory('sessionFactory', SessionFactory);

	/**
	 * @class Conciliador.SessionFactory
	 * Serviço para gerenciamento da sessão do usuário
	*/
	function SessionFactory() {

		var STR_SESSION_NAME = 'control-session';
		var STR_SESSION_DATA = STR_SESSION_NAME + '-data';

		var strSessionToken = storage.getItem(STR_SESSION_NAME);
		var objSessionUser = storage.getItem(STR_SESSION_DATA);

		return {
			create: Create,
			destroy: Destroy,
			getToken: GetToken,
			getUser: GetUser,
			isAuthenticated: IsAuthenticated
		};

		/**
		 * @method Create
		 * Cria a sessão do usuário
		 * @param {String} strToken token de acesso do usuário
		 * @param {Object} objUser objeto com os dados do usuario e sua lista de pvs
		 */
		function Create(strToken, objUser) {
			if (strToken && objUser) {
				storage.setItem(STR_SESSION_NAME, strToken);
				strSessionToken = strToken;

				if (objUser) {
					storage.setItem(STR_SESSION_DATA, JSON.stringify(objUser));
					objSessionUser = objUser;
				}

				return true;
			}

			return false;
		}

		/**
		 * @method Destroy
		 * Limpa a sessão do usuário
		 */
		function Destroy() {
			storage.removeItem(STR_SESSION_NAME);
			storage.removeItem(STR_SESSION_DATA);
			strSessionToken = null;
			objSessionUser = null;
		}

		/**
		 * @method GetToken
		 * Retorna o token do usuário registrado na sessão
		 */
		function GetToken() {
			return strSessionToken || storage.getItem(STR_SESSION_NAME);
		}

		/**
		 * @method GetUser
		 * Retorna os dados do usuário registrados na sessão
		 */
		function GetUser() {
			objSessionUser = typeof objSessionUser === 'string' ? JSON.parse(objSessionUser) : objSessionUser;
			return objSessionUser || JSON.parse(storage.getItem(STR_SESSION_DATA));
		}

		/**
		 * @method IsAuthenticated
		 * Verifica se o usuário esta autenticado
		 */
		function IsAuthenticated() {
			return !!GetToken();
		}
	}
})(window.localStorage);
